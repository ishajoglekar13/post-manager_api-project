//const path = require('path');
//
//module.exports = {
//    entry: {
//        app: ['@babel/polyfill', './src/app.js'] //jidhar se compile karna hai
//    },
//    output: {
//        path: path.resolve(__dirname, "dist"), //absolute path will be given by dirname
//        filename: 'app.bundled.js'
//    },
//    module: { //all third party modules
//        rules: [
//            {
//                test: /\.js$/,
//                exclude: /(node_modules)/, //excluding the node modules
//                use: {
//                    loader: 'babel-loader',
//                    options: {
//                        presets: ['@babel/preset-env'] //additional info
//                    }
//                }
//            }
//        ]
//    },
//    devServer: {
//        //        publicPath: '/dist/', //final karna padega ki kidhar banana hai
//        contentBase: path.join(__dirname, "dist"), //static content jidhar pada hpota hai index.html join is absolute path
//        watchContentBase: true, //watcher
//        open: true //by default open nai karta isliye we use open : true
//    }
//}

const path = require('path');
module.exports = {
    entry : {
        app : ['@babel/polyfill','./src/app.js']
    },
    output: {
        path : path.resolve(__dirname, "dist"),
        filename: 'app.bundled.js'
    },
    //watch:true,
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use:{
                    loader: 'babel-loader',
                   options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    },
    devServer: {
//        publicPath: '/dist',
        contentBase: path.join(__dirname, "dist"),
        watchContentBase:true,
        open:true
    }
}






